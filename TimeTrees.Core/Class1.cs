﻿namespace TimeTrees.Core
{
    public class User { }
    public class DeviceInfo { }

    public interface IDbProvide
    {
        User GetUser(int id);
        User GetUser(string userName);
        DeviceInfo GetDeviceInfo(int userID);
    }

    public class SQLLiteProvider : IDbProvide
    {
        public DeviceInfo GetDeviceInfo(int userID)
        {
            throw new NotImplementedException();
        }

        public User GetUser(int id)
        {
            throw new NotImplementedException();
        }

        public User GetUser(string userName)
        {
            throw new NotImplementedException();
        }
    }
}