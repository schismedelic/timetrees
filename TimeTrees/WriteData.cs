﻿using System;
using Newtonsoft.Json;
using System.IO;
using TimeTrees;

class WriteData
{
    public static void WritePeopleJSON(Person[] array, string path)
    {
        string json = JsonConvert.SerializeObject(array, Formatting.Indented);
        File.WriteAllText(path, json);
    }

    public static void WriteTimelineJSON(Timeline[] array, string path)
    {
        string json = JsonConvert.SerializeObject(array, Formatting.Indented);
        File.WriteAllText(path, json);
    }
}