﻿using System;
using System.Globalization;
using TimeTrees;

class DateParse
{
    public static DateTime Parse(string line)
    {
        DateTime date;
        if (!DateTime.TryParseExact(line, "yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
        {
            if (!DateTime.TryParseExact(line, "yyyy-MM", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
            {
                if (!DateTime.TryParseExact(line, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
                {
                    date = default;
                }
            }
        }
        return date;
    }
}