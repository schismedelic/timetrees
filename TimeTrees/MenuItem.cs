﻿using System;
using TimeTrees;

internal class MenuItem
{
    public string Name { get; set; }

    public Action Function { get; set; }

    public MenuItem(string name, Action function)
    {
        this.Name = name;
        this.Function = function;
    }

    public override string ToString() => Name;
}