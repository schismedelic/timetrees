﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using TimeTrees;

namespace TimeTrees
{
    class Program
    {
        static List<MenuItem> ListAdd()
        {
            List<MenuItem> menuItems = new List<MenuItem>();
            menuItems.Add(new MenuItem("Добавить человека", () => { }));
            menuItems.Add(new MenuItem("Добавить событие", () => { }));
            menuItems.Add(new MenuItem("Изменить данные человека", () => { }));
            menuItems.Add(new MenuItem("Люди, рождённые в високосный год", () => { }));
            menuItems.Add(new MenuItem("Разница между событиями", () => { }));
            menuItems.Add(new MenuItem("Выход", () => Environment.Exit(0)));

            return menuItems;
        }

        static void Main(string[] args)
        {
            Console.CursorVisible = false;
            Menu menu = new Menu(ListAdd());
            menu.MenuItem();

            Person[] peopleDate = null;
            Timeline[] timeline = null;
            Console.WriteLine("Выберете с чем работать: json или csv");

            string input = Console.ReadLine();
            Console.Clear();

            if (input == "json")
            {
                peopleDate = ReadData.ReadPersonJson().ToArray();
                timeline = ReadData.ReadTimelineJson().ToArray();
            }
            if (input == "csv")
            {
                peopleDate = ReadData.ReadDataPeopleCsv();
                timeline = ReadData.ReadDataTimelineCsv();
                WriteData.WritePeopleJSON(peopleDate, "..\\..\\..\\..\\people.json");
                WriteData.WriteTimelineJSON(timeline, "..\\..\\..\\..\\timeline.json");
            }

            DeltaDate(timeline);
            foreach (var people in GetPeoples(peopleDate))
            {
                Console.WriteLine(people);
            }
        }

        static DateTime GetMaxDate(Timeline[] timeline)
        {
            DateTime maxDate = DateTime.MinValue;
            foreach (var line in timeline)
            {
                DateTime date = line.Date;
                if (date > maxDate)
                {
                    maxDate = date;
                }
            }
            return maxDate;
        }

        static DateTime GetMinDate(Timeline[] timeline)
        {
            DateTime minDate = DateTime.MaxValue;
            foreach (var line in timeline)
            {
                DateTime date = line.Date;
                if (date < minDate)
                {
                    minDate = date;
                }
            }
            return minDate;
        }

        static void DeltaDate(Timeline[] timeline)
        {
            DateTime maxDate = GetMaxDate(timeline);
            DateTime minDate = GetMinDate(timeline);

            int years = maxDate.Year - minDate.Year;
            int months = maxDate.Month - minDate.Month;
            int days = maxDate.Day - minDate.Day;

            Console.WriteLine($"Между максимальной и минимальной датами прошло {years} лет, {months} месяцев и {days} дней");
        }

        static List<string> GetPeoples(Person[] people)
        {
            DateTime today = DateTime.Today;
            List<string> result = new List<string>();
            foreach (var line in people)
            {
                DateTime datePeople = line.BirthDate;
                DateTime delta = new DateTime().Add(today.Subtract(datePeople));
                if (DateTime.IsLeapYear(datePeople.Year) && delta.Year <= 20)
                {
                    result.Add(line.Name);
                }
            }
            return result;
        }
    }
}