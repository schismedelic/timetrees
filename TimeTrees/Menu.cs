﻿using System;
using System.Collections.Generic;
using TimeTrees;

internal class Menu
{
    List<MenuItem> menuItems = new List<MenuItem>();

    public Menu(List<MenuItem> menuItems) => this.menuItems = menuItems;

    public void DrawMenu(int row, int column, int index)
    {
        Console.SetCursorPosition(column, row);
        for (int i = 0; i < menuItems.Count; i++)
        {
            if (i == index)
            {
                Console.BackgroundColor = Console.ForegroundColor;
                Console.ForegroundColor = ConsoleColor.Black;
            }
            Console.WriteLine(menuItems[i]);
            Console.ResetColor();
        }
        Console.WriteLine();
    }

    public void MenuItem()
    {
        int row = Console.CursorTop;
        int column = Console.CursorLeft;
        int index = 0;

        while (true)
        {
            DrawMenu(row, column, index);
            switch (Console.ReadKey(true).Key)
            {
                case ConsoleKey.DownArrow:
                    if (index < menuItems.Count - 1)
                        index++;
                    break;
                case ConsoleKey.UpArrow:
                    if (index > 0)
                        index--;
                    break;
                case ConsoleKey.Enter:
                    menuItems[index].Function();
                    break;
            }
        }
    }
}