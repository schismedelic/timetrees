using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using TimeTrees;

class ReadData
{
    public static List<Person> ReadPersonJson()
    {
        string jsonPerson = File.ReadAllText("..\\..\\..\\..\\people.json");
        return JsonConvert.DeserializeObject<List<Person>>(jsonPerson.Replace(';', ' '));
    }

    public static List<Timeline> ReadTimelineJson()
    {
        string jsonTimeline = File.ReadAllText("..\\..\\..\\..\\timeline.json");
        return JsonConvert.DeserializeObject<List<Timeline>>(jsonTimeline.Replace(';', ' '));
    }

    public static Timeline[] ReadDataTimelineCsv()
    {
        string pathss = "..\\..\\..\\..\\timeline.csv";
        string[] lines = File.ReadAllLines(pathss);
        Timeline[] splitData = new Timeline[lines.Length];

        int i = 0;
        foreach (var line in lines)
        {
            string[] words = line.Split(';');
            var description = "";
            if (words.Length == 1)
            {
                description = "";
            }
            else
            {
                description = words[1];
            }
            splitData[i++] = new Timeline(DateParse.Parse(words[0]), description);
        }
        return splitData;
    }

    public static Person[] ReadDataPeopleCsv()
    {
        string pathh = "..\\..\\..\\..\\people.csv";
        string[] lines = File.ReadAllLines(pathh);
        Person[] personInfoSplit = new Person[lines.Length];

        int i = 0;
        foreach (var line in lines)
        {
            string[] words = line.Split(';');
            DateTime deathDate;
            if (words.Length == 3)
            {
                deathDate = default;
            }
            else
            {
                deathDate = DateParse.Parse(words[3]);
            }
            personInfoSplit[i++] = new Person(int.Parse(words[0]), words[1], DateParse.Parse(words[2]), deathDate);
        }
        return personInfoSplit;
    }
}