﻿using System;

struct Person
{
    public int ID;
    public string Name;
    public DateTime BirthDate;
    public DateTime DeathDate;

    public Person(int id, string name, DateTime birthDate, DateTime deathDate)
    {
        this.ID = id;
        this.Name = name;
        this.BirthDate = birthDate;
        this.DeathDate = deathDate;
    }
}