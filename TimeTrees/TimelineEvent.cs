﻿using System;
using System.Collections.Generic;

struct Timeline
{
    public DateTime Date;
    public string Description;
    public Timeline(DateTime Date, string Description)
    {
        this.Date = Date;
        this.Description = Description;
    }
}